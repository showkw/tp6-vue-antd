
const webpack = require('webpack');
const path = require( 'path' );

function resolveRealPath( dir ) {
	return path.join( __dirname, dir )
}

const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
// 代码压缩
const UglifyJsPlugin = require( 'uglifyjs-webpack-plugin' );
// gzip压缩
const CompressionWebpackPlugin = require( 'compression-webpack-plugin' );

// 是否为生产环境
const isProduction = process.env.NODE_ENV !== 'development';

//本地资源打包前缀 默认/ 如有CDN 则填写CDN地址
const cdnUri = '/';

const loadCdn = {
	css: [
		'https://at.alicdn.com/t/font_511605_bljx3o7uywc.css',
		'https://cdn.bootcss.com/normalize/8.0.1/normalize.min.css',
		'https://cdn.bootcss.com/animate.css/3.7.2/animate.min.css',
	],
	js: [
		'https://cdn.bootcss.com/vue/2.6.10/vue.min.js',
		'https://cdn.bootcss.com/vue-router/3.1.3/vue-router.min.js',
		'https://cdn.bootcss.com/vuex/3.1.1/vuex.min.js',
		'https://cdn.bootcss.com/core-js/2.6.9/core.min.js',
		'https://cdn.bootcss.com/fastclick/1.0.6/fastclick.min.js',
		'https://cdn.bootcss.com/lodash.js/4.17.15/lodash.min.js',
		'https://cdn.bootcss.com/axios/0.19.0/axios.min.js',
		'https://cdn.bootcss.com/Swiper/4.5.1/js/swiper.min.js',
		'https://cdn.bootcss.com/vue-lazyload/1.3.2/vue-lazyload.js',
		'https://cdn.bootcss.com/moment.js/2.24.0/moment.min.js',
	],
	externals:{
		'vue': 'Vue',
		'vuex': 'Vuex',
		'vue-router': 'VueRouter',
		'axios': 'axios',
		'lodash': '_',
		'swiper': 'Swiper',
		'vue-lazyload': 'VueLazyload',
		'moment': 'moment',
		'core-js':'core-js',
		'fastclick':'FastClick'
	},
};
const outputDir = 'public';
process.env.npm_config_report = true;
module.exports = {
	lintOnSave: false,
	outputDir: outputDir,
	runtimeCompiler: true,
	publicPath: isProduction?(cdnUri && cdnUri !== '/'?cdnUri:'/'):'/',
	assetsDir: '',
	filenameHashing: true,
	productionSourceMap: false,
	pages: {
		index: {
			// page 的入口
			entry: 'resources/main.js',
			// 模板来源
			template: 'resources/index.html',
			// 在 dist/index.html 的输出
			filename: 'index.html',
			// 当使用 title 选项时，
			// template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
			// title: '',
			// 在这个页面中包含的块，默认情况下会包含
			// 提取出来的通用 chunk 和 vendor chunk。
			cdn: loadCdn,
		}
	},
	
	chainWebpack: config => {
		config.resolve.symlinks( true );
		config.resolve.alias
			.set( 'vue$', 'vue/dist/vue.esm.js' )
			.set( '@assets', resolveRealPath( 'resources/assets' ) )
			.set( '@components', resolveRealPath( 'resources/components' ) )
			.set( '@config', resolveRealPath( 'resources/config' ) )
			.set( '@plugins', resolveRealPath( 'resources/plugins' ) )
			.set( '@mixins', resolveRealPath( 'resources/mixins' ) )
			.set( '@router', resolveRealPath( 'resources/router' ) )
			.set( '@service', resolveRealPath( 'resources/service' ) )
			.set( '@utils', resolveRealPath( 'resources/utils' ) )
			.set( '@views', resolveRealPath( 'resources/views' ) );
		
		// https://github.com/webpack-contrib/webpack-bundle-analyzer
		if ( process.env.npm_config_report ) {
			config
				.plugin( 'webpack-bundle-analyzer' )
				.use( require( 'webpack-bundle-analyzer' ).BundleAnalyzerPlugin );
		}
		
		const splitOptions = config.optimization.get( 'splitChunks' );
		config.optimization.splitChunks(
			Object.assign( {}, splitOptions, {
				// （缺省值5）按需加载时的最大并行请求数
				maxAsyncRequests: 16,
				// （默认值3）入口点上的最大并行请求数
				maxInitialRequests: Infinity,
				// （默认值：1）分割前共享模块的最小块数
				minChunks: 1,
				// （默认值：30000）块的最小大小
				minSize: 20000,
				// webpack 将使用块的起源和名称来生成名称: `vendors~main.js`,如项目与"~"冲突，则可通过此值修改，Eg: '-'
				automaticNameDelimiter: '-',
				// cacheGroups is an object where keys are the cache group names.
				name: true,
				cacheGroups: {
					default: false,
					common: {
						name: `chunk-common`,
						minChunks: 2,
						maxInitialRequests: 5,
						minSize: 0,
						priority: 2,
						chunks: 'initial',
						reuseExistingChunk: true
					},
				}
			} )
		);
		
		
		config.module
			.rule( 'images' )
			.use( 'image-webpack-loader' )
			.loader( 'image-webpack-loader' )
			.options( { bypassOnDebug: true } )
			.end();
		
		//将pub目录下必须得文件移动到public
		config.plugin( 'copy' ).use( new CopyWebpackPlugin( [
			{
				from: resolveRealPath( 'pub' ),
				to: resolveRealPath( outputDir ),
				force: true
			}
		] ) );
		
	},
	css: {
		extract: true,
		sourceMap: false,
		loaderOptions: {
			sass: {
				data: `
          @import "@assets/style/common/config.scss";
         `
			},
			less: {
				javascriptEnabled: true, //less 配置
				modifyVars: {
					'primary-color': '#007bff',
					'link-color': '#515a6e',
					'link-hover-color': '#007bff',
					'link-active-color': '#007bff',
				},
			}
		},
		modules: false
	},
	configureWebpack: config => {
		config.performance = {
			hints: 'warning',
			//入口起点的最大体积 整数类型（以字节为单位）
			maxEntrypointSize: 50000000,
			//生成文件的最大体积 整数类型（以字节为单位 300k）
			maxAssetSize: 30000000,
			//只给出 js 文件的性能提示
			assetFilter: function ( assetFilename ) {
				return assetFilename.endsWith( '.js' );
			}
		};
		config.externals = loadCdn.externals;
		if( isProduction ){
			// 代码压缩
			config.plugins.push(
				new UglifyJsPlugin({
					uglifyOptions: {
						//生产环境自动删除console
						compress: {
							// warnings: false, // 若打包错误，则注释这行
							drop_debugger: true,
							drop_console: true,
							pure_funcs: ['console.log']
						}
					},
					sourceMap: false,
					parallel: true
				})
			);
			
			// gzip压缩
			const productionGzipExtensions = ['html', 'js', 'css'];
			config.plugins.push(
				new CompressionWebpackPlugin({
					filename: '[path].gz[query]',
					algorithm: 'gzip',
					test: new RegExp(
						'\\.(' + productionGzipExtensions.join('|') + ')$'
					),
					threshold: 10240, // 只有大小大于该值的资源会被处理 10240
					minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
					deleteOriginalAssets: false // 删除原文件
				})
			);
			
			config.plugins.push(
				new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
			);
			
		}
	},
	devServer: {
		disableHostCheck: true,
		// port: 80, // 端口号
		// host: 'sso.xmr.com', // 'localhost'
		https: false, // https:{type:Boolean}
		open: true, //配置自动启动浏览器
		proxy: {
			'/api': {
				target: 'http://sso.xmr.com',
				ws: true,
				changeOrigin: true
			},
		}, // 配置多个代理
	}
};
