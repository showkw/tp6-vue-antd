<?php
/**
 * BaseFunction.php
 * CopyRight (C) 2019 https://www.xiaomoren.com
 * Author  Showkw <showkw@163.com>
 * Date   2019/10/11 10:35
 */

namespace xmr\helper;


/**
 * Class BaseFunction
 *
 * @package xmr\helper
 * @author Showkw <showkw@163.com>
 * @date   2019/10/11 10:35
 */
class BaseFunction
{
	/**
	 * @author Showkw <showkw@163.com>
	 **/
	public static function de( $var = null, $var_dump = false )
	{
		self::dd( $var, $var_dump );
		exit();
	}

	/**
	 * @author Showkw <showkw@163.com>
	 **/
	public static function dd($var = null, $var_dump = false)
	{
		echo '<pre>';
		$var_dump = empty( $var ) ? true : $var_dump;
		if ( $var_dump ) {
			var_dump( $var );
		} else {
			print_r( $var );
		}
	}

	/**
	 * * 生成唯一UUID
	 *
	 * @author   Showkw <showkw@163.com>
	 * @param bool $is_eql
	 * @throws
	 * @return string
	 */
	public static function getUUID( $is_eql = true )
	{
		$str = $is_eql ? '%04X%04X-%04X%04X-%04X%04X-%04X%04X' : '%04X%04X%04X%04X%04X%04X%04X%04X';

		return sprintf( $str, random_int( 0, 65535 ),
			random_int( 0, 65535 ), random_int( 0, 65535 ), random_int( 16384, 20479 ),
			random_int( 32768, 49151 ), random_int( 0, 65535 ), random_int( 0, 65535 ), random_int( 0, 65535 ) );
	}


	/**
	 * @author Showkw <showkw@163.com>
	 **/
	public static function routeToFront(  )
	{
		return view(app()->getRootPath().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'index.html');
	}

    /*
     *
     * hash加密
     *
     * @param   string   $string  待加密字串
     * @return  json  状态码以及状态码对应提醒信息
     */
    public static function hash_string( $string )
    {
        return password_hash( $string, 1 );
    }

    /*
     *
     * 验证hash加密
     *
     * @param   string   $string  验证字串
     * @param   string   $hash   待验证hash
     * @return  boolean
     */
    public static function hash_check( $string, $hash )
    {
        return password_verify( $string, $hash );
    }
}
