<?php
/**
 * TokenUtil.php
 * CopyRight (C) 2019 http://www.xiaomoren.com
 * Author  Showkw <showkw@163.com>
 * Date   2019/10/14 14:22
 */

namespace xmr\util;


/**
 * Class TokenUtil
 *
 * @package xmr\util
 * @author Showkw <showkw@163.com>
 * @date   2019/10/14 14:22
 */
class TokenUtil
{
	/**
	 * 生成token令牌
	 *
	 * @param string $custom_value 自定义值
	 * @author   Showkw <showkw@163.com>
	 * @return string 返回生成的token令牌字符串
	 **/
	public static function makeTokenString( string $custom_value = null ):string
	{
		return md5( uniqid( $custom_value?:'' . time(), true ) );
	}
}
