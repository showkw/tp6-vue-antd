<?php
/**
 * cross.php
 * CopyRight (C) 2019 http://www.xiaomoren.com
 * Author  Showkw <showkw@163.com>
 * Date   2019/10/11 11:27
 */
return [
	'header'=>[
		'Access-Control-Allow-Methods'=>'GET,POST,OPTIONS',
		'Server'=>'XiaoMoRen SSO Server',
		'X-Powered-By'=>'SSO.XiaoMoRen.Com',
	],
	'allow'=>[

	],
];
