<?php
/**
 * api.php
 * CopyRight (C) 2019 https://www.XiaoMoRen.com
 * Author  Showkw <showkw@163.com>
 * Date   2019/10/11 11:35
 */

use app\middleware\ApiAuthMiddleware;
use think\facade\Route;

Route::group('api',function(){
	Route::post('version',function(){
		return \xmr\response\Code::send(\xmr\response\Code::SUCCESS,['version'=>'v1.0.0.0']);
	});

})->middleware(ApiAuthMiddleware::class)
	->allowCrossDomain(\think\facade\Config::get( 'cross.header' ))
	->method('post');
