<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
use xmr\helper\BaseFunction;

//404
Route::miss(function(){
	return redirect('/404');
});

Route::group('/',function(){

	//404路由
	Route::get('404','Index/miss')->name('p404');

	//首页/默认页
	Route::get('',function (){
		return BaseFunction::routeToFront();
	});


})->allowCrossDomain(\think\facade\Config::get( 'cross.header' ));

