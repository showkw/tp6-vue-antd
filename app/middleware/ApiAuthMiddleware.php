<?php

namespace app\middleware;


use think\Request;
use xmr\helper\BaseFunction;
use xmr\response\Code;

class ApiAuthMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
		if( $request->isPost() ){

			$data = $request->param(false);
			$param_time = $sign = $sign_data = null;
			foreach( $data as $key=>$value ){
				if( $key === '_t' ){
					$param_time = $value;
					continue;
				}
				if( $key === '_sign' ){
					$sign = $value;
					continue;
				}
				$sign_data[$key]=$value;
			}
			$request_time = $request->time();
			//检查请求时间是否过期
			if( $request_time - $param_time >= 10*60 || $request_time < $param_time ){
				Code::send(Code::INVALID_REQUESTS_TIMESTAMP);
			}
		}
		return $next($request);
    }
}
