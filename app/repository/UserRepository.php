<?php
/**
 * UserRepository.php
 * CopyRight (C) 2019 http://www.xiaomoren.com
 * Author  Showkw <showkw@163.com>
 * Date   2019/10/11 14:17
 */

namespace app\repository;


use app\model\User;
use xmr\repository\AbstractRepository;

/**
 * Class UserRepository
 *
 * @package app\repository
 * @author Showkw <showkw@163.com>
 * @date   2019/10/11 14:17
 */
class UserRepository
{

	/**
	 * @author Showkw <showkw@163.com>
	 * @throws
	 **/
	public function getUserByMobile( string $mobile )
	{
		return User::where('mobile',$mobile)->find();
	}
}
