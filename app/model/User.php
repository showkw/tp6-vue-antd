<?php
/**
 * UserModel.php
 * CopyRight (C) 2019 http://www.xiaomoren.com
 * Author  Showkw <showkw@163.com>
 * Date   2019/10/11 14:00
 */

namespace app\model;


use think\Model;

/**
 * Class UserModel
 *
 * @package app\model
 * @author Showkw <showkw@163.com>
 * @date   2019/10/11 14:00
 */
class User extends Model
{
	// 设置当前模型对应的完整数据表名称
	protected $table = 'user';

	protected $pk = 'uid';

}
