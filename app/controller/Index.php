<?php
namespace app\controller;

use app\BaseController;

class Index extends BaseController
{
    public function index()
    {
        return view(app()->getRootPath().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'index.html');
    }

	/**
	 * @author Showkw <showkw@163.com>
	 **/
	public function miss()
	{
		return view(app()->getRootPath().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'404.html');
    }

}
