import consts from '@service/const'
export default {
	version:{
		name: 'version',
		method: 'POST',
		desc: '版本检查',
		path: '/api/version',
		mockPath: '/api/version',
		mock: false,
		debug: true,
		params: {},
	},
};
