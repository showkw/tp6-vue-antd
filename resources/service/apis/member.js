export default {
	normalSignIn:{
		name: 'normalSignIn',
		method: 'POST',
		desc: '普通密码登录接口',
		path: '/api/login/normalSignIn',
		mockPath: '/api/login/normalSignIn',
		mock: false,
		debug: true,
		params: {
			mobile: null,
			password: null,
		},
	},
}
