/**
 * /*
 * 通用路由配置，需要放在配置项数组的末端
 *
 * @format
 */

export default [
  {
    path: '/',
    meta: {
	    ignoreApi: true,
	    ignoreAuth: true,
	    keepAlive:true,
    },
	  component: () => import('@views/Home.vue'),
  },
	// {
	// 	path: '',
	// 	meta: {
	// 		ignoreApi: true,
	// 		ignoreAuth: true,
	// 		keepAlive:true,
	// 	},
	// 	component: () => import('@views/Home.vue'),
	// },
	{
		path: '/sign',
		meta: {
			ignoreApi: true,
			ignoreAuth: true,
			keepAlive:false,
		},
		component: () => import('@views/Sign.vue'),
	},
]
