import Vue from 'vue'
import App from './App.vue'
import router from '@plugins/router'
import store from '@plugins/store'
import Filters from '@plugins/filters'
import inject from '@plugins/inject'
import Cookies from '@plugins/cookies'
import FastClick from 'fastclick'
import VueMeta from 'vue-meta';
import '@assets/style/index.scss';
import '@plugins/antd';
import 'ant-design-vue/dist/antd.less'
import VueLazyload from 'vue-lazyload';

//全局组件自动注册
import '@plugins/components';


Vue.config.productionTip = false;
const lang = Cookies.get( 'lang' ) || 'zh-cn';
Vue.config.lang = lang;

for ( let k in Filters ) {
	Vue.filter( k, Filters[ k ] );
}
Vue.use( inject );
Vue.use( VueMeta );

Vue.use(VueLazyload);

if ( 'addEventListener' in document ) {
	document.addEventListener( 'DOMContentLoaded', function () {
		FastClick.attach( document.body )
	}, { passive: true } )
}

G.vbus =new Vue( {
	router,
	store,
	render: h => h( App )
} ).$mount( '#app' );
