import Vue from 'vue'
import VueRouter from 'vue-router'
import RoutesMap from '@router'
import { ROUTER_DEFAULT_CONFIG } from '@config'
import { routerBeforeEachFunc, routerAfterEachFunc } from '@config/interceptors/router'

if (!window.VueRouter) Vue.use(VueRouter);

// 注入默认配置和路由表
let routerInstance = new VueRouter( {
	...ROUTER_DEFAULT_CONFIG,
	routes: RoutesMap
} );
// 注入拦截器
routerInstance.beforeEach( routerBeforeEachFunc );
routerInstance.afterEach( routerAfterEachFunc );

G.router = routerInstance;

export default routerInstance
