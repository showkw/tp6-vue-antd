import axios from './axios'
import api from './api'
import apiList from '@service/apis'
import consts from './const'
import _ from 'lodash'
import { message, Modal, notification } from "ant-design-vue";


export default {
	install: ( Vue, options ) => {
		// 需要挂载的都放在这里
		Vue.prototype.$api = api
		Vue.prototype.apis = apiList
		Vue.prototype.$ajax = axios
		Vue.prototype.$const = consts
		Vue.prototype.$_ = _
		Vue.prototype.$message = message;
		Vue.prototype.$notification = notification;
		Vue.prototype.$info = Modal.info;
		Vue.prototype.$success = Modal.success;
		Vue.prototype.$error = Modal.error;
		Vue.prototype.$warning = Modal.warning;
		Vue.prototype.$confirm = Modal.confirm;
	}
}
