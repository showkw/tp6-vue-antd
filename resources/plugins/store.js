import Vue from 'vue'
import Vuex from 'vuex'
import { VUEX_DEFAULT_CONFIG } from '@config'
import modules from '@service/store'

if (!window.Vuex) Vue.use( Vuex );

export default new Vuex.Store( {
	...modules,
	...VUEX_DEFAULT_CONFIG
})
