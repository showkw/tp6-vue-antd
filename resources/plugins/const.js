import { CONST_DEFAULT_CONFIG } from '@config'
import CONST_CONFIG from '@service/const'


class MakeConst {
	constructor( options ) {
		this.const = {};
		this.constBuilder( options )
	}
	
	constBuilder( {
		              config = []
	              } ) {
		Object.keys( config ).map( namespace => {
			this._constSingleBuilder( { namespace, config: config[ namespace ] } )
		} )
	}
	
	_constSingleBuilder( {
		                     namespace,
		                     config = {}
	                     } ) {
		this.const[ namespace ] = {};
		Object.keys( config ).map(( cst,value) => {
			Object.defineProperty( this.const[ namespace ], cst, { value } )
		} );
	}
}


export default new MakeConst( {
	config: CONST_CONFIG,
	...CONST_DEFAULT_CONFIG
} )[ 'const' ]

