let _ = require( 'lodash' )

/**
 * [nameKey 命名空间]
 * @type {String}
 */
let nameKey = 'ZS_'

/**
 * [_get 获取 localstroage 值]
 * @param  {[String]}   key [key 值]
 * @return {[All]}       [value 值]
 */
export function _get( key ) {
	if ( !key ) return
	let data = JSON.parse( localStorage.getItem( nameKey + key ) )
	return data && data.data
}

/**
 * [_set 存储 localstroage 值]
 * @param  {[String]}   key   [key 值]
 * @param  {[All]}   value [value 值]
 */
export function _set( key, value ) {
	if ( !key ) return
	let data = {
		data: value
	}
	try {
		localStorage.setItem( nameKey + key, JSON.stringify( data ) )
	} catch ( e ) {
		// console.log(e);
	}
}

/**
 * [_remove 删除内存中的数据]
 * @param  {[Object]}   keys [内存中的 key]
 */
export function _remove( keys ) {
	if ( !key ) return
	if ( _.isArray( keys ) ) {
		keys.map( function ( item ) {
			localStorage.removeItem( nameKey + item )
		} )
	} else if ( typeof( keys ) === 'string' ) {
		localStorage.removeItem( nameKey + keys )
	}
}

/**
 * [_clear 清除所有的缓存数据]
 */
export function _clear() {
	localStorage.clear()
}

