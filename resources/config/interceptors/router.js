
import {NODE_ENV,BASE_API_URL} from "../index";

export function routerBeforeEachFunc (to, from, next) {
    // 这里可以做页面拦截，很多后台系统中也非常喜欢在这里面做权限处理
    // window.G.vbus.$vux.loading.show()
    if( to.matched.length === 0 ){
        if(NODE_ENV === 'prod'){
            window.location.href=to.path;
        }else{
            window.location.href=BASE_API_URL+to.path;
        }
    }else{
        next()
    }
    
   
}
export function routerAfterEachFunc (to, from) {
    // 这里可以做页面拦截，很多后台系统中也非常喜欢在这里面做权限处理
	// window.G.vbus.$vux.loading.hide()
}
